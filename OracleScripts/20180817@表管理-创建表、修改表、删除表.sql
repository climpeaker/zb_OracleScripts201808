/*--------------------------------
一：创建、修改、删除表结构
  对表结构的操作。
  
1、创建表结构
   语法1：
       create table 用户名.表名 (列1 列1数据类型,列2 列2数据类型,... );
   解析：
       （1） 用户名.表名 说明该表是属于某一个用户；
             如果当前用户有权限，可以在其权限之内的用户空间下建立一个表；
             如果当前用户在自己空间下建立表，则可以省略用户名；
   语法2：
        create table 用户名.表名(字段列表) as select子查询；
   解析：
        （1）如果表名后面没有字段列表，则使用子查询的字段名作为新建表的字段名；
        （2）如果表名后面有字段列表，则使用表名后面的字段列表作为新建表的字段名，
             且要求个数和子查询的个数要保持一致；
        （3）子查询的数据会在创建完新表之后，将输入插入到新表中；

--------------------------------*/
--- create建立表
create table depts (
dept_id number(4),
dept_name varchar2(20),
manager_fname  varchar2(20),
dept_loc  varchar2(30)
)

--- 使用 as 子查询建立一个表
create table depts2 as select * from departments;

select * from  depts2;

create table depts3(dept_id,dept_name,manager_id,location_id) as select * from departments;

select * from  depts3;

create table depts3(dept_id,dept_name,manager_id,location_id) as select * from departments;
