/*--------------------------------
一：创建、修改、删除表结构【DDL】
  对表结构的操作。
  
1、创建表结构
   语法1：
       create table 用户名.表名 (列1 列1数据类型,列2 列2数据类型,... );
   解析：
       （1） 用户名.表名 说明该表是属于某一个用户；
             如果当前用户有权限，可以在其权限之内的用户空间下建立一个表；
             如果当前用户在自己空间下建立表，则可以省略用户名；
   语法2：
        create table 用户名.表名(字段列表) as select子查询；
   解析：
        （1）如果表名后面没有字段列表，则使用子查询的字段名作为新建表的字段名；
        （2）如果表名后面有字段列表，则使用表名后面的字段列表作为新建表的字段名，
             且要求个数和子查询的个数要保持一致；
        （3）子查询的数据会在创建完新表之后，将输入插入到新表中；

2、修改表结构
   主要是对已经创建的表的结构进行字段添加、字段类型修改、删除字段等操作；
   (1) 新增字段
   语法：
        alter table 用户名.表名 
              add (字段名 字段类型,字段名,字段类型...)
              
   (2) 修改字段类型
   语法：
       alter table 用户名.表名 
             modify(字段名 字段类型 默认值);
   解析：
       可以修改字段的数据类型；如果有数据则修改之后的数据类型必须兼容已有的数据；
       修改默认值，则不影响之前已有的数据，只对后续插入的数据有影响；          
   (3)删除字段
    语法：
       alter table 用户名.表名 
             drop (字段列表); 
3、删除表结构
   将表结构从数据库中删除，如果表中有数据，则连同数据一块删除；
   语法：
       drop table  用户名.表名;             
    注意：
        删除的是表结构；
4、重名对象名
   对oracle中的对象（表，视图，序列，索引，函数，存储过程，触发器，游标...）进行重命名；
   语法：
        rename 用户名.旧对象名 to 用户名.新对象名;
        
5、截断表
   删除表中所有的数据记录；
   语法：
       truncate table 用户名.表名;
   注意：
       会将表中的所有的数据全部删除；
       注意 truncate 命令属于ddl；
   扩展作业：
       ddl的truncate命令和dml的delete命令的异同点；【P111】
       (1)都是删除数据
       (2)delete是dml语言，支持事务处理；
       (3)truncate是ddl语言，不支持事务处理；
       (4)truncate执行效率比delete高；

二：新增、修改、删除表数据【DML】
1、新增数据记录
   向已有的表中插入一条数据记录；
   语法：
       insert into 用户名.表名 [要插入数据的字段列表]
              values(插入的值列表)
    解析：
        （1）要插入的字段列表和值列表要一一对应；【数据类型和个数】
        （2）要插入的数据的字段列表可以省略，如果省略则插入的值与表中的所有的字段保持一一对应关系；
              此种方式在实际项目中，不推荐使用；
        （3）如果字段设置了默认值，如果没有插入值，则使用默认值；
        （4）插入的字段值类如果是日期类型，建议使用日期转换函数进行处理；   
    语法2：
       values部分可以使用select子查询替代，这样可以一次性插入多行数据记录；    
       insert into 用户名.表名 [要插入数据的字段列表]
              select 子查询
    解析：
        （1）要插入数据的字段列表和select 子查询中显示的数据列表字段个数和数据类型要一一对应，名字可以不一致；
        （2）可以一次性插入多条数据记录；

2、修改数据记录
   修改已有的数据记录中的某些列的值；
   语法：
       update 用户名.表名 set 列名=列值,列明=列值... where 修改限定条件
   解析：
       （1）where修改限定条件一定要有，否则，会修改整张表中所有的数据记录；【危险操作，谨慎处理】
       （2）set中的列值可以是子查询的结果。（单行单列子查询）

3、删除数据记录
   删除已有的数据记录，不会改变表结构。【表结构不变，删除数据记录】
   语法：
       delete 用户名.表名  where 删除的限定条件
   解析：
       （1）删除的限定条件一定要有，否则会删除整张表中所有的数据记录；【危险操作，谨慎处理】
       
三、事务的概念和应用
1、事务概念
   在一个完整的业务处理中，会有多步处理进行分步执行，只有当每一步处理都执行成功时，整个业务逻辑处理才算成功；
   也就是，这个业务处理中的每一步构成一个整体；如果其中某一步出现错误，则只能回到业务开始的节点；【回滚处理】
   总之，在这个业务逻辑中，只有两种状态，成功【结束状态】或失败【开始状态】。没有中间状态；
   
   事务处理要求：要么全部成功，要么全不成功；【事务只有两种状态】
2、事务的4个特性
    cadi：
    a：原子性（不可分割性）；
    c：一致性；
    i：隔离性；
    d：持久性；
    具有这个四个特性的业务处理，才会构成事务；  
     
3、事务的应用
   在事务的应用中一般常用的两个操作：
   回滚：
       当事务处理过程中发生错误，则会回滚到事务开始时的状态；
       rollback;
   提交： 
       当事务完成之后，进行提交以后，事务结果则不能在改变；
       commit;
   注意：
       只有提交到数据库服务器的事务结果才会被固化；
       dml操作受事务保护；【新增、修改、删除】  
       
   关于事务的编程，在jdbc时再做处理； 

4、事务扩展
   死锁、加锁、脏数据；
   
四、数据库约束
   数据库约束是为了保证数据实体的完整性，保证dml操作的合法性，已经保证表与表之间的关系完整性，
   而定义的限定条件是就约束；
   常用的约束有：
     非空约束：not null
     唯一性约束：unique【都可以作为候选键】
     主键约束：primary key 【一个关系表中只能有一个主键，但是可以有多个候选键】
     外键约束：foreign key
     自定义约束：check
     
   约束根据定义的位置，可以分为表级约束和字段级约束；
   字段级约束是写在创建表时的字段后面；
   表级约束是写在创建表之后的语句； 
   语法：
       create table 用户名.表名(
       字段名  字段类型  default 默认值  字段级约束,
       字段名  字段类型  default 默认值  字段级约束,
       字段名  字段类型  default 默认值  字段级约束,
       constraint 表级约束,
       constraint 表级约束n
       ) 
    
1、非空约束
   使用关键字 not null 进行限定；
   被限定的字段不能够使用null值；
   只能定义在字段级别上；
   创建表时添加非空约束：
   语法：
       create table 用户名.表名(
       字段名  字段类型  default 默认值 [constraint 约束名] not null,
       字段名  字段类型  default 默认值  not null,
       字段名  字段类型  default 默认值       
       )     
   修改表结构式添加非空约束：
   语法：
       alter table  用户名.表名 
             modify 字段名 字段类型 default 默认值  not null; 
   解析：
       （1）可以使用alter table 的modify 添加not null 约束    
       （2）如果被添加的非空约束的字段之前有数据，要求已有的数据必须满足not null要求，才能添加；
       （3）添加的约束可以使用  constraint 约束名 给添加的约束命名；
           【推荐使用该种方式，好处：可以通过约束名对其进行修改或删除】
       （4）约束的命名习惯：表名_字段名_约束简称或约束名
       （5）如果省略约束命名，则系统会自动为其进行命名；
   扩展作业：
       如何查询当前用户下所建立的约束对象信息；
       
2、唯一性约束
    使用关键字 unique 对字段进行唯一性限定；
    要求该字段中的每个值都具有唯一性，不能够重复；
    唯一性约束的字段可以有任意多个null值；
    唯一性约束可以在表级和字段级进行定义；【同一个字段只能二选一】
    
    字段级语法：
        create table 用户名.表名(
       字段名  字段类型  default 默认值 [constraint 约束名] unique, ---字段级约束
       字段名  字段类型  default 默认值,
       字段名  字段类型  default 默认值       
       )
                
    表级语法：
        create table 用户名.表名(
       字段名  字段类型  default 默认值,
       字段名  字段类型  default 默认值,
       字段名  字段类型  default 默认值,
       constraint 约束名 unique(字段列表) --- 表级约束      
       )
       
       
--------------------------------*/
--- create建立表
create table depts (
dept_id number(4),
dept_name varchar2(20),
manager_fname  varchar2(20),
dept_loc  varchar2(30)
)

--- 使用 as 子查询建立一个表
create table depts2 as select * from departments;

select * from  depts2;

create table depts3(dept_id,dept_name,manager_id,location_id) as select * from departments;

select * from  depts3;

create table depts3(dept_id,dept_name,manager_id,location_id) as select * from departments ;---where 1=2;

---- 修改表结构 
-- 新增字段
alter table depts3 
      add (location_name varchar2(20));

-- 修改字段
alter table depts3
      modify(location_name varchar2(50) default '天津市河西区宾水道天津市政府');      
-- sql plus 中执行以下命令可以查看表结构      
--desc depts3;      
-- 删除字段
alter table depts3
      drop (location_name,location_id);      
      
--- 删除表结构
-- 删除表depts3
drop table depts3;      
      
--- 截断表
--创建一个表
create table depts4(dept_id,dept_name,manager_id,location_id) as select * from departments ;      
-- 查询表中数据
select * from depts4;      
-- 截断表
truncate table depts4;      
      

------------准备数据    
-- 创建一张表  
create table depts5(dept_id,dept_name,manager_id,location_id) as select * from departments ;  
-- 新增一个字段
alter table depts5 
      add (location_name varchar2(20));
-- 修改字段          
alter table depts5
      modify(location_name varchar2(50) default '天津市河西区宾水道天津市政府');        
------------准备数据        
-------------DML---------------------   
select * from depts5;   
-- 插入数据（不推荐此种方式）
insert into hr.depts5 values(
       120,'后勤部',250,3000,'天津西青'
);
--- 插入数据（推荐）
insert into depts5 (dept_id,dept_name,manager_id,location_id)
       values(130,'开发部',260,3010);
-- 使用select子查询进行数据插入，可以插入多行数据       
insert into depts5 (dept_id,dept_name,manager_id,location_id)
       select * from departments where department_id = 200;
       
--- 修改数据       
select * from depts5;         
-- 修改部门id为130的部门名称为开发一部，地址为天津市西青区中北镇；
update depts5 set dept_name = '开发一部',location_name = '天津市西青区中北镇'  where dept_id = 130;
       
-- 删除数据
select * from depts5;

delete from depts5 where dept_id = 200;       
-- 提交事务
commit;       
--------------------------完整性约束-------------------------------    
select * from depts5;   
--- 非空约束
alter table depts5
modify dept_id not null;

-- 如果被添加的非空约束的字段之前有数据，要求已有的数据必须满足not null要求，才能添加
alter table depts5
modify manager_id not null;

--- 唯一性约束
--字段级语法：
create table depts6 (
dept_id number(4) constraint depts6_deptId_uiq unique,
dept_name varchar2(20),
manager_fname  varchar2(20),
dept_loc  varchar2(30)
)

-- 验证唯一性(执行两次即可)
insert into depts6 (dept_id) values (110);

-- 表级约束
create table depts7 (
dept_id number(4) ,
dept_name varchar2(20),
manager_fname  varchar2(20),
dept_loc  varchar2(30),
constraint depts7_deptId_uiq  unique(dept_id)
)

-- 验证唯一性(执行两次即可)
insert into depts7 (dept_id) values (110);






