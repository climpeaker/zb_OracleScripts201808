/*--------------------------------
零、语法要求
1、sql语句不区分大小写，大小写不敏感；
   sql语法构成的关键字或表名、字段名等大小写不敏感；
   但是字段里面的内容是区分大小写，还有字符串表达式中也是区分大小写的；
  sql语句一行的结束符是 ; ，一行可以分多行书写，但是以分号作为结束;
2、命名规范：
    表名、字段名一般使用前缀或后缀的方式进行命名；例如：t_emp、t_stuName、empTable、stuNameCol等
    防止和sql语句中的关键字冲突，以及避免系统表中一些常用的表名或字段名；
    例如：name、users、user属于系统表，名或系统字段名；
    书写规范：
        一般通过大小写分开的方式，将sql语句的关键字和用户的信息内容进行区分；例如：
        SELECT * FROM t_emp;
    sql语句的注释：
        sql语句可以使用 -- 进行单行注释；
                   使用 /*  * / 进行单行或多行注释
3、Oracle中使用 "" 标识字符串；mysql、sqlserver使用 '' 或者 "" 都可以标识字符串
   Oracle中单引号与双引号用法有区别；
4、Oracle数据库是二维关系数据库
二维关系数据：
   行和列组成；
   列是不可再分的原子结构；
   行具有唯一标示性；
   同列中的数据的类型都是相同的；
一、学习目标
1、select查询
2、排序
二、笔记
1、查询
1.1 select 语句查询
    语法：
           select *|字段列表|表达式  from 表名 where 
                 查询条件 [order by 字段1 asc|desc,字段2 asc|desc 字段n asc|desc]；
           
    解析：
           （1）select后面表示的是查询结果集中需要显示的字段名；
                * 表示显示查询表中所有的字段列表；
                如果只显示几个地段，则直接列出即可，字段之间使用,分割；
                可以执行表达式；
           （2）from 后面是要查询的表名，可以是多个表，也是用,分割；
           （3）where后面跟的是查询条件；
           （4）order by 表示查询结果需要进行排序的规则：
                asc表示升序排列【默认】；desc表示降序排列；
           
           
1.2 where条件语句
    where语法：
        where 字段名 操作符  条件表达式
    解析：
        where条件中的操作符有：
        比较运算：
        =  >  <  >= <= <> != ^=
        between ... and ...  等价于：字段>=... and  字段<= ...
        in(取值列表)
        like 条件表达式【模糊查询】
        is null 和  is not null 【判定字段是否为null，注意：null和空的区别】
        逻辑运算：
           把多个比较运算进行逻辑关联：
           not  and  or （等价于java的非与或）
        
        运算优先级：
           参考教材P47.
        Like的模糊查询：
           使用like可以进行模糊查询，需要时通配符进行模糊匹配；
           % 通配0个或多个字符，任意字符（包括中文）；
           _ 通配1字符（包括中文）
           通配符表达式以字符串的形式使用；例如： '王%明' ；  '王_明'
        not否定搭配：
           not可以和 in 、like、 exists、between、null进行搭配
        and和or的用法：
           and 表示与，表示两个条件同时满足；
           or 表示或，表示两个条件有一个满足即可；
1.2 order语句排序
    （1）order by 后面可以跟多个字段进行排序，每个字段有两种选择 asc 升序（默认），desc 降序
    （2）排序字段可以分为第一排序字段、第二排序字段。。。
    （3）不同类型的排序字段的排序规则：
             数字类型、日期类型、字符串（字典排序）
             null值的处理，升序asc在最后，降序desc在最前；           
    （4）order by 语句一般是在查询语句的最后         
    （5）order by 后面处理可以使用字段名之外，可以使用数字表示使用select之后的第几个子段进行排序；（不推荐使用）  
--------------------------------*/
---select查询示例
select * from departments; -- * 的用法
select department_id,department_name,manager_id from departments; -- 查询列表
select * from departments where department_id >= 60; -- where 条件 比较运算
select * from departments where  department_id between 60 and 100; -- between and 用法
select * from departments where department_id >=60 and department_id <= 100; -- 上式等价写法
select * from departments where department_id in (60,70,80,90,100); -- in 用法
select * from departments where manager_id is null; -- is null 用法
select * from departments where manager_id is not null; -- is not null 用法
select * from departments where department_name like '%n'; -- like 模糊查询
select * from departments where department_name not like '%n'; -- not like 模糊查询
select * from departments where  department_id not between 60 and 100; -- not between and 用法（不包括边界值）
select * from departments where department_name like '%n' and department_name like '_d%'; -- and 用法
select * from departments where department_name like '%n' or department_name like '_d%'; -- and 用法

select * from departments order by department_name desc ,manager_id; -- order by 排序
select * from departments order by 3 desc,2 desc; -- order by 使用字段顺序作为排序标准（不推荐使用）
select * from departments order by manager_id desc,department_name desc; -- order by 排序
select * from departments order by manager_id asc; -- asc null值在最后
select * from departments order by manager_id desc; -- desc null值在最前

select * from departments where DEPARTMENT_NAME = 'NOc' -- 字段内容或字符串表达式区分大小写


