/*--------------------------------
一、函数
1 函数的概念
  数据库引擎中内置了一些进行通过功能处理的函数；【不同的数据库函数略有区别】
2、函数的分类
  根据函数处理的数据记录条数，分为：
    单行函数：
      每一行都进行处理，然后对每一行都输出结果；
      每一行数据有一个处理结果；
      字符函数、数字函数、通用函数、转换函数、日期函数等等属于单行函数
    多行函数：
      需要对多行数据进行处理，然后输出一个结果；
      多行数据有一个结果；
      组函数是多行函数；
3、单行函数
3.1 字符函数
    对字符串处理的函数；
    lower、upper、initcap、concat、substr、length、instr、replace、lpad、rpad
3.2 数字函数
    对数值的处理；
    round、trunc、mod
3.3 日期函数
    可以通过 sysdate获取Oracle数据的当前日期；
    months_between、add_month、next_day、last_date、round、trunc
3.4 转换函数
    进行类型和格式转换的函数；
    to_char、to_date、to_number
    （1） 项目中，日期类型需要转为指定格式字符串进行处理
    （2）to_date中，两个参数之间要匹配，也就是，格式串是第一个参数的描述；
        即：第二个参数格式串要和第一个参数日期字符串保持一致
   （3）在进行日期比较时，需要将日期的格式进行统一，然后再进行比较大小；
        一般是，将日期转换为同一格式的字符串然后再比较大小；
                或者是，全部使用to_date转为date类型进行比较；
3.5 通用函数
    公共处理函数；
    nvl(exp1,exp2)：给null设置默认值；如果exp1为null，则取exp2的值作为结果；
    nvl2(exp1,exp2,exp3):如果exp1不为null，则取exp2的值；否则取exp3的值作为结果；
    coalesce(exp1,exp2,exp3,...expn):从最左侧exp1开始返回第一个不为null的表达式的值作为结果；
    
    补充：
        case表达式：
            语法：（switch-case用法）
                 case exp0 when exp10 then exp11
                           when exp20 then exp21
                           when exp30 then exp31
                           else exp100
                           end                           
            解析：
                 类似java的switch-case语句；
                 当exp0的值等于exp10的值，则取exp11作为结果，
                 依次类推，如果没有匹配，则最后去exp100作为结果；
                 
            语法：（if-else用法）
                 case when 条件表达式1 then 结果表达式1
                           when 条件表达式2 then 结果表达式2                          
                           else else语句部分
                           end
        DECODE函数
            语法：
                 decode(exp0,
                           exp10,exp11,
                           exp20,exp21,
                           exp30,exp31,
                           exp100);
            解析：
                decode函数是case表达式的简化写法；
                
4、多表查询
   就是多个表之间通过关联条件进行联合查询的方式；
   查询显示的结果是一个临时的缓存的内容；（没有存储到物理结果中，断电丢失）
   与被查询的物理表不一样；（物理表存储在物理结果中，断电不会丢失） 
   
   根据查询结果集的内容选取方式可以分为：内连接和外连接；
  内连接：
      多表联合查询结果集中仅包含满足条件的记录，就是内连接；
      常用的内连接有：
          等值连接、不等值连接；
  外连接：
      除内连接之外的多表联合查询都是外连接；
      也就是，外连接的结果集中除了满足条件之外的记录还有其他记录；
      常用的外连接：
          左外连接、右外连接、全外连接、笛卡尔积；
     
4.1 笛卡尔积
   是两个或多个表之间没有条件直接进行关联；一般没有实际的意义；
   总记录条数是两个表的记录条数之积；
   
4.2 等值连接
    等值连接是进行多表关联查询时，两个表之间的关联字段的值做相等连接；
（1）where等值连接
    在where条件中进行两个表之间的字段进行等值关联；
    注意：
        【1】多表关联查询时，在select之后的字段列表中，
        如果某个字段在两个都出现时，需要使用表前缀进行区分，避免二义性错误；
        【2】显示列表中使用单独的 * 表示显示所有的列，
        如果还有其他的列需要和*并列使用，则*需要使用前缀；
        【3】等值连接条件与其他的过滤条件之间需要使用 and 进行关联，否则没有实际的意义；
（2）自然连接的等值连接
     自然连接使用关键字 natural join 完成；
     
     语法：
         select 字段列表  from 表1 别名1 natural join 表2 别名2 where条件语句；
     注意：
         （1）要求等值连接的字段在两个表中的字段名要一致且数据类型要兼容；   
         （2）如果两个表中有多个相同的字段，则所有相同的字段都会作为等值匹配的条件；
         （3）select之后的字段列表中不能使用 * ；
         
（3）使用using子句作为等值连接         
    使用using指出两个表中的需要进行关联的字段名；【弥补自然连接不能指定字段缺点】     
    语法：
        select 字段列表 from 表1 别名1 join 表2 别名2 using(关联字段名) where 条件语句；     
    注意：
        using后面的字段不能有表前缀，因为他指定的是两个表中都有的用于等值连接的字段名；
        如果using有多个字段进行等值连接，则使用,分割即可；
        字段列表不能使用*匹配符；
        
（4）使用join...on...等值连接
     语法：
         select 字段列表  from 表名1 别名1 join 表名2 别名2 on 等值连接条件  where 条件；     
     注意：
         可以使用*号通配符了；

【补充】
    自连接查询
    自关联查询就是在一个表内部进行等值连接的查询。
    一般用于上下级的结构的查询；
【补充】
    非等值连接
    等值连接是在一个范围内进行匹配叫做非等值连接；
    一般使用 between...and... 作为匹配限定；
【补充】 fm、fx、th、sp格式串的控制元素
     fm：提出空白字符和前导0，例如：06月剔除为 6月； to_char
     fx：字符串和描述字符串的格式串要严格匹配，包括符号和空白字符；to_date
     
     th：使用英文的序数进行表示；to_char
     sp：数字以英文的形式显示；一般将spth加在数字后面，将其转换为英文序数形式；例如：1 --> first                 
     to_char使用
--------------------------------*/
--- 字符函数
-- sql中涉及下标的地方都是从1开始，java从0开始；
select substr('jfldsajfloewaf',4), substr('jfldsajfloewaf',4,5) from dual; 

select dept.*,substr(department_name,1,5) as shortName from departments dept;
select dept.*,lpad(substr(department_name,1,5),5,'*') as shortName  from departments dept;
--- 数字函数
-- 第二个参数，如果是正数是对小数点后面的小数处理，
-- 如果是负数则是对小数点前面的整数处理，整数处理完成之后，需要使用0进行占位，也就是整数部分的位数不能少；
select round(56789.8765,2),round(56789.8765,-2),trunc(56789.8765,2),trunc(56789.8765,-2) from dual;
--- 日期函数
select sysdate from dual;
select next_day(sysdate,'星期二'),last_day(sysdate) from dual;
select round(sysdate,'day'),round(sysdate,'year'),trunc(sysdate,'day'),trunc(sysdate,'year') from dual;
--- 转换函数
--- to_char：日期、数字转换为指定格式的字符串
select sysdate from dual;
--- 在自定义格式串中，需要使用""将非格式串的内容引出
select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),
to_char(sysdate,'"现在报时：现在是"yyyy"年"mm"月"dd"日 东方时间"hh24"时"mi"分"ss"秒"') from dual;
select d.*,to_char(manager_id,'L099,999,999') from departments d;
-- to_number 将字符串转换为数字
-- 注意，格式类型只能是数值类型，也就是转换的结果是数值对，所有不能用字符串的数字格式
select to_number('8238','99999.99') ,--to_number('128031sa','$99,999.99'),
to_number(to_char(sysdate,'yyyy'),'99999') from dual;
-- to_date 将指定格式的字符串转换为日期类型
-- 第二个参数格式串要和第一个参数日期字符串保持一致；
select to_date('8月 15 2018','mon dd yyyy') from dual;
select to_char(to_date('2018年8月15日','yyyy"年"mm"月"dd"日"'),'yyyy-mm-dd hh24:mi:ss') from dual;

--【补充】 fm、fx、th、sp格式串的控制元素
select to_char(sysdate,'yyyy-mm-ddspth hh24:mi:ssspth') from dual;
select to_char(sysdate,'fmyyyy-mm-dd hh24:mi:ss') from dual;
select to_date('8月 15 2018','fxmon  dd yyyy') from dual;
--- 通用函数
select d.*,nvl(manager_id,0) newManager_id from departments d;

select * from departments where nvl(manager_id,0)+1000 > 100;

select d.*,nvl2(manager_id,manager_id,'-1') newManager_id from departments d;

-- case 表达式（switch-case用法）
select d.*,case nvl(manager_id,0) when manager_id then '经理【'||manager_id||'】' 
                           when 0 then '没有经理' end as newManager_id from departments d;
                           
-- case 语句用法（if-else用法）
select d.*,case when manager_id is not null then '经理【'||manager_id||'】' 
                           when manager_id is null then '没有经理' end as newManager_id 
                             from departments d;
-- decode函数
select d.*,decode( nvl(manager_id,0), 
                           manager_id,'经理【'||manager_id||'】', 
                            0 ,'没有经理' ) as newManager_id from departments d;

select * from employees;
--==============================================================--
-- 笛卡尔积
select * from departments; --
select * from employees; --
select * from departments d,employees e;
--- 等值连接
-- where等值连接
-- 查询员工姓名以A开头，并且显示该员工的部门信息；
select * from employees e where e.last_name like 'A%';

select * from departments d,employees e where d.department_id = e.department_id 
       and e.last_name like 'A%'; --- where等值连接条件与其他条件要使用and连接
select * from departments d,employees e where d.department_id = e.department_id 
       or e.last_name like 'A%';
-- 自然连接
select * from departments;
select * from Employees;
-- 查询员工姓名以A开头，并且显示该员工的部门信息；
-- 因为 有manager_id和department_id两个同名的字段，所有这两个字段都做了等值匹配的条件
select d.department_name,e.employee_id,e.last_name from departments d natural join employees e 
where e.last_name like 'A%';
--- 等价于
select * from departments d,employees e 
       where d.department_id = e.department_id and d.manager_id= e.manager_id
             and e.last_name like 'A%';

-- using等值连接
select * from departments;
select * from Employees;
-- 查询员工姓名以A开头，并且显示该员工的部门信息；
select d.department_name,e.employee_id,e.last_name 
       from departments d join employees e using(department_id) where  e.last_name like 'A%';
       
select d.department_name,e.employee_id,e.last_name 
       from departments d join employees e using(department_id,manager_id) where  e.last_name like 'A%';

-- on等值连接
select * from departments;
select * from Employees;
-- 查询员工姓名以A开头，并且显示该员工的部门信息；

select d.*,e.* from departments d join employees e on d.department_id = e.department_id 
       where   e.last_name like 'A%';

select d.*,e.* from departments d 
       join employees e on d.department_id = e.department_id and d.manager_id = e.manager_id
            where   e.last_name like 'A%';

--- 自连接查询
-- 查询员工的姓名及其领导的姓名
select * from employees;
--- 查询结果有重复，领导的领导是他自己【使用exists关键字可以进行优化】
select e.employee_id, e.first_name,e.last_name,
       m.first_name as manager_first_name,m.last_name as manager_last_name
        from employees e ,employees m where e.manager_id = m.manager_id;

--- 非等值连接
--- 查询
select * from employees;
select * from egrade;

--- 进行非等值匹配
--- 查询员工的工资，并显示员工工资等级；
select e.*,eg.egradename from employees e,egrade eg 
       where e.salary between eg.egrademin and eg.egrademax;
