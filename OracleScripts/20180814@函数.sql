/*--------------------------------
一、select执行表达式和函数
  1、字段和表别名
    可以给select查询语句中的字段列表中的字段起别名；
    可以给from之后的表起别名；
    字段别名语法：
        字段/表达式  as  别名;
     注意：
        as 关键字可以省略；
    表别名：
        表名  别名；
    注意：
        oracle中表别名没有as关键字；mysql、SQLServer中可以有as关键字，并且可以省略；
    补充：
        * 表示所有的字段；
        如果有其他表或者是其他表达式也作为显示结果字段，则不能单独使用*表示该表的所有的字段，
        则需要使用 表别名.* 表示所有的字段；
            
  2、执行函数和表达式
  （0）dual虚拟表
    Oracle提供了一张虚拟表dual，用于在select执行表达式函数时，满足必须有from条件；可以使用具体的表满足form要求；
  （1）执行表达式
  语法：
    select 表达式 from 表名 [where];
  注意：
    表达式中如果有操作数为null ，则整个表达式的结果为null；
    所以当进行字段运算时，需要使用函数处理该字段是否为null情况；

  3、Oracle中单引号和双引号的区别
     （1）单引号
         单引号表示引用字符串的字面值；【表示字符串表达式】；
         单引号可以转移单引号；例如： '''' 表示输出一对 '' ；
         在表达式计算中都是使用''的字符串；
     （2）双引号
         双引号引住关键字，表示作为一个普通字符串使用；【一般用于别名操作中】
         双引号引住表名、字段名，表示表名、字段名要严格区分大小写；【需要后续扩展】
     扩展说明：
         Oracle对sql语句进行解析时会默认将其全部全换位大写，然后再进行进行执行；
         如果使用create创建一个表，即便写的是小写的表名，Oracle实际创建的表名也是大写的；
         但是如果创建表时，表名使用了""引住，则Oracle会创建一个与""中的内容严格大小写的表名；
         
   4、||连字符操作符   
      在Oracle中可以使用 ||将两个字符串或者是字段内容进行连接；
      如果有null则作为空字符串进行连接，注意结果不是null；
      
   5、distinct关键字
      作用是提出查询结果中重复的行；  
      是否重复是取决于显示结果中的所有的字段都必须重复；【必须是一行中所有的字段都重复】    
--------------------------------*/
select 1+1 from dual;
select null +1 from dual; -- 表达式中如果有操作数为null ，则整个表达式的结果为null
select '123'+1 as "departments" from dual; -- 会将数字字符串转换为数字进行处理；
select "123"+1 from dual;
select '123a456'+1 from dual;
select 'a'+1 from dual;
select ' '+1 from dual;
select '''''' from dual;
-- 如果有其他表或者是其他表达式也作为显示结果字段，则不能单独使用*表示该表的所有的字段，
-- 则需要使用 表别名.* 表示所有的字段；
-- 当进行字段运算时，需要使用函数处理该字段是否为null情况；
select dep.*,manager_id + 10000  as newManager_id from departments dep; 
-- 双引号引住表名、字段名，表示表名、字段名要严格区分大小写；
select dep.*,manager_id + 10000  as newManager_id from "departments" dep; 
-- Oracle默认创建的是大写的表名；
select dep.*,manager_id + 10000  as newManager_id from "DEPARTMENTS" dep;  
-- || 连接操作符
select dep.*,manager_id + 10000 || '部门'  as newManager_id from departments dep; 

select dep.*,department_name || '_'||department_id|| ' of '||manager_id as newDept from departments dep; 

-- distinct关键字
select distinct manager_id from departments dep; 
select * from departments dep; 
select distinct location_id,manager_id from departments dep; 
