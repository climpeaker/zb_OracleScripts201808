/*--------------------------------
一、多表关联查询
  根据查询结果集的内容选取方式可以分为：内连接和外连接；
  内连接：
      多表联合查询结果集中仅包含满足条件的记录，就是内连接；
      常用的内连接有：
          等值连接、不等值连接；
  外连接：
      除内连接之外的多表联合查询都是外连接；
      也就是，外连接的结果集中除了满足条件之外的记录还有其他记录；
      常用的外连接：
          左外连接、右外连接、全外连接、笛卡尔积；
二、外连接
1、左外连接
   左连接就是以左表作为主表，右表作为从表，结果集中包含主表中的所有的记录，
   从表通过等值匹配条件匹配主表中的记录；当从表没有可与主表匹配的记录时，使用 null 行进行匹配；
   
   sql实现：
       可以使用 left (outer) join 
       还可以使用频度 (+) 进行实现；
   
2、右外连接
   右连接就是以右表作为主表，左表作为从表，结果集中包含主表中的所有的记录，
   从表通过等值匹配条件匹配主表中的记录；当从表没有可与主表匹配的记录时，使用 null 行进行匹配；
   
   sql实现：
       可以使用 right (outer) join 
       还可以使用频度 (+) 进行实现；
       
3、全外连接
   将左连接和右连接的结果集合并在一起，然后剔除重复的行即可；
   
   sql实现：
       可以使用 full (outer) join 
  
   注意：outer关键字可以省略；
         如果使用(+)表示外连接，(+)修饰的这一端是从表。
         【+ 表示1次或多次重复；】

--==************************************************==--
三、子查询
1、子查询概念
    sql中的查询是缓存在内存中，是临时的；但是有具有表结构的性质；
   子查询就是使用已有的查询结果作为被查询表、显示字段、条件值进行的再次查询；
    
   如果子查询结果集作为条件值进行使用，根据子查询的结果的记录条数，可分为单行子查询和多行子查询；
       单行子查询：
           子查询结果至多有一条记录，且只能有一个字段；
       多行子查询：
           子查询结果有多条记录，且只能有一个字段；
2、单行子查询
   至多只能有一行记录，且只有一个列；
   
3、多行子查询
   使用的子查询的结果集中有多行记录；一般会搭配any、all进行查询；
   any（some）：任一一个，如果子查询中有多个值可以选择，只有有一个满足条件；
   all：所有的；如果子查询中有多个值可以选择，所有的值都要满足条件；
   常见的搭配有：
   <any 等价于：< max
   >any 等价于：> min
   =any 等价于：in
   <all 等价于：< min
   >all 等价于：>max
   =all(没有意义)
   

四、组函数和分组查询
1、组函数
   组函数是多行函数；
   多行函数：操作多条数据记录，获取一条结果值；
   单行函数：每一条记录都有一个结果值；
   常用的组函数：
       avg：平均值；
       sum：求和；
       max：最大值；
       min：最小值；
       count：统计记录条数【注意对null值的处理】
       stddev：标准差；
       variance：方差；
       
   注意：
       分组函数对null值的处理；
       注意null值对分组函数的影响；
   解析：
       分组函数在处理是会自动忽略过滤掉null值；
       如果分组函数中也需要处理null值时，则需要使用nvl函数先对null进行处理；
       
2、分组查询
   
   导入问题：
       查询各个部门的员工的平均工资、最高工资、最低工资？
    分组查询：
        将被操作数据记录按照分组条件划分为若干组，然后对每组进行操作（组函数），作为分组查询的结果记录集；
    语法：
        select 显示列表或分组函数  from  表名 where 过滤条件  
               group by 分组字段列表  having 分组过滤条件 order by 排序字段列表
    解析：
        （1）分组查询中的显示类表中只能是分组函数或者是group by 之后的排序字段，不能是其他的字段；
        （2）having子句只能搭配group by 使用；
        （3）having子句中的分组过滤条件使用的字段只能是group by 之后的排序字段或者是分组函数；
        （4）having过滤的是分组之后处理的结果集；
        （5）如果一个过滤条件即可写在where部分也可以写在having部分，则优先where部分过滤【性能较好】
        （6）执行流程：先执行 where -- groupby --- 分组函数 --- having  --- order by
     
     练习：
         查询各个部门的员工的平均工资、最高工资、最低工资，显示平局工资大于5000的部门？   
        
--------------------------------*/
select * from departments;
select * from employees;
--- 左连接
select d.*,e.* from departments d left outer join employees e on d.department_id = e.department_id;
-- 等价于
select d.*,e.* from departments d,employees e where d.department_id = e.department_id(+);

--- 等价于
select d.*,e.* from employees e right outer join departments d on d.department_id = e.department_id;


--- 右连接
select d.*,e.* from departments d right outer join employees e on d.department_id = e.department_id;
-- 等价于
select d.*,e.* from departments d,employees e where d.department_id(+) = e.department_id;

--- 全连接
select d.*,e.* from departments d full outer join employees e on d.department_id = e.department_id;

---------------------------
select * from departments;
select * from employees;

-- 子查询
-- 查询员工所在的地址编号为1700的员工信息、部门名称；
-- 等值连接查询
select e.*,d.* from departments d inner join employees e on d.department_id = e.department_id 
       where d.location_id = 1700;
-- 子查询
select t.*  from 
       (select e.*,d.department_name,d.location_id 
               from departments d inner join employees e on d.department_id = e.department_id ) t
                    where t.location_id = 1700;

-- 查询it部门中比James Landry（first_name last_name）的工资还高的员工信息
-- 查询james的工资
select e.salary from employees e 
       where lower(e.first_name) = lower('james') and lower(e.last_name) = lower('Landry')
-- 查询比james工资高的it员工信息
select * from employees e ,departments d 
       where e.department_id = d.department_id and upper(d.department_name) = upper('it')
             and e.salary > 
                 (select e.salary from employees e 
                       where lower(e.first_name) = lower('james') and lower(e.last_name) = lower('Landry'))

--- 查询地址编号为1700的部门中的最高工资和最低工资以及平均工资；
select max(salary) "max_salary",min(salary) "min_salary",avg(nvl(salary,0)) as "avg_salary"  
       from (select e.*,d.department_name,d.location_id 
                    from departments d inner join employees e on d.department_id = e.department_id ) t
                         where t.location_id = 1700;

--- 在显示字段中进行子查询
--- 查询60部门的平均工资，以及该部门的所有的员工信息；
select (select avg(nvl(salary,0)) from employees e where e.department_id = 60) as "dep_avg_salary",
       e.* from employees e where e.department_id=60;       

--- 下面不能运行，因为分组函数不能和费分组字段并列显示
select avg(nvl(salary,0)),e.* from employees e where e.department_id = 60

---- 多行子查询
select * from departments;
select * from employees;
-- 查询公司Marketing部门员工工资比IT部门最高工资还要高的员工信息；
--- 1 查询it部门的最高工资
select max(salary) from departments d ,employees e 
       where d.department_id = e.department_id and upper(d.department_name)=upper('it')
--- 2 查询Marketing部门员工信息,且比it部门最高工资高

--- 单行子查询
select * from departments d ,employees e 
       where d.department_id = e.department_id  and upper(d.department_name)=upper('Marketing')
             and e.salary >(
                    select max(salary) from departments d ,employees e 
                         where d.department_id = e.department_id and upper(d.department_name)=upper('it')
                            )

--- 等价于  > all  --- 多行子查询
select * from departments d ,employees e 
       where d.department_id = e.department_id  and upper(d.department_name)=upper('Marketing')
             and e.salary > all(
                    select salary from departments d ,employees e 
                         where d.department_id = e.department_id and upper(d.department_name)=upper('it')
                            )

-- 查询公司Marketing部门员工工资比IT部门工资还要高的员工信息；
--- 多行子查询
select * from departments d ,employees e 
       where d.department_id = e.department_id  and upper(d.department_name)=upper('Marketing')
             and e.salary > any(
                    select salary from departments d ,employees e 
                         where d.department_id = e.department_id and upper(d.department_name)=upper('it')
                            )
--- 单行子查询
select * from departments d ,employees e 
       where d.department_id = e.department_id  and upper(d.department_name)=upper('Marketing')
             and e.salary >(
                    select min(salary) from departments d ,employees e 
                         where d.department_id = e.department_id and upper(d.department_name)=upper('it')
                            )
-- 查询公司Marketing部门员工工资比IT部门最低工资还要低的员工信息；
--- 多行子查询
select * from departments d ,employees e 
       where d.department_id = e.department_id  and upper(d.department_name)=upper('Marketing')
             and e.salary < all(
                    select salary from departments d ,employees e 
                         where d.department_id = e.department_id and upper(d.department_name)=upper('it')
                            )
--- 单行子查询
select * from departments d ,employees e 
       where d.department_id = e.department_id  and upper(d.department_name)=upper('Marketing')
             and e.salary < (
                    select min(salary) from departments d ,employees e 
                         where d.department_id = e.department_id and upper(d.department_name)=upper('it')
                            )
-- 查询公司Marketing部门员工工资比IT部门工资还要低的员工信息；
--- 多行子查询
select * from departments d ,employees e 
       where d.department_id = e.department_id  and upper(d.department_name)=upper('Marketing')
             and e.salary < any(
                    select salary from departments d ,employees e 
                         where d.department_id = e.department_id and upper(d.department_name)=upper('it')
                            )
--- 单行子查询
select * from departments d ,employees e 
       where d.department_id = e.department_id  and upper(d.department_name)=upper('Marketing')
             and e.salary < (
                    select max(salary) from departments d ,employees e 
                         where d.department_id = e.department_id and upper(d.department_name)=upper('it')
                            )
-- 课下作业：
---查询公司Marketing部门员工工资比IT部门平均工资还要低的员工信息；【注意null值的处理】
---查询公司Marketing部门员工工资和IT部门员工工资相同的员工信息；【in、=any】


--------------------------
-- 分组函数
select * from employees;

select count(*),count(manager_id),avg(salary),max(salary),
       max(manager_id),min(salary),sum(salary),sum(manager_id)
         from employees;

select count(*),count(manager_id),count(nvl(manager_id,0)),
       avg(manager_id),sum(manager_id)/count(manager_id),
         sum(manager_id)/count(*),avg(nvl(manager_id,0)) from employees;

---分组查询
select * from employees;
select * from departments;

-- 简单导入： 查询IT部门平均工资、最高工资、最低工资？
select avg(nvl(salary,0)) as avg_salary,max(nvl(salary,0)) as max_salary,min(nvl(salary,0)) as min_salary
       from employees;
       
-- 查询各个部门的员工的平均工资、最高工资、最低工资？
select e.department_id,d.department_name,avg(nvl(salary,0)) as avg_salary,
       max(nvl(salary,0)) as max_salary,min(nvl(salary,0)) as min_salary
                          from employees e,departments d where e.department_id=d.department_id 
                               group by e.department_id,d.department_name;

-- 查询部门编号大于90的各个部门的员工的平均工资、最高工资、最低工资，并显示部门id和部门名？

-- where条件过滤【效率高】
select e.department_id, d.department_name,avg(nvl(salary,0)) as avg_salary,
       max(nvl(salary,0)) as max_salary,min(nvl(salary,0)) as min_salary
                          from employees e,departments d 
                               where e.department_id=d.department_id and d.department_id>90
                                     group by e.department_id,d.department_name;
                                     
-- having条件过滤
select e.department_id, d.department_name,avg(nvl(salary,0)) as avg_salary,
       max(nvl(salary,0)) as max_salary,min(nvl(salary,0)) as min_salary
                          from employees e,departments d 
                               where e.department_id=d.department_id
                                     group by e.department_id,d.department_name 
                                           having e.department_id>90;

---查询各个部门的员工的平均工资、最高工资、最低工资，显示平局工资大于5000的部门？   
--- having条件过滤
select e.department_id, d.department_name,avg(nvl(salary,0)) as avg_salary,
       max(nvl(salary,0)) as max_salary,min(nvl(salary,0)) as min_salary
                          from employees e,departments d 
                               where e.department_id=d.department_id --- 等值连接
                                     group by e.department_id,d.department_name --- 分组查询
                                           having avg(nvl(salary,0))>5000;  --- 分组过滤

--- 不适用having过滤,使用子查询
select * from (
    select e.department_id, d.department_name,avg(nvl(salary,0)) as avg_salary,
       max(nvl(salary,0)) as max_salary,min(nvl(salary,0)) as min_salary
                          from employees e,departments d 
                               where e.department_id=d.department_id --- 等值连接
                                     group by e.department_id,d.department_name --- 分组查询
              ) t where  avg_salary > 5000;  --- where过滤条件
