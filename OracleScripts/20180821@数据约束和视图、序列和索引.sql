/*--------------------------------
一、完整性约束
1、非空约束
   使用not null，只能是列级约束；
2、唯一性约束
   使用unique，可以是列级或表级约束；
3、主键约束
   从候选键中选出一个作为主键；
   主键：数据记录的唯一标识符；
   一个表中至多只能有一个主键，但是可以有多个候选键；
   主键约束要求主键值不能为null；【Oracle不需要显式约束not null】
   
   列级约束语法：
       create table 用户名.表名(
       字段名  字段类型  default 默认值 [constraint 约束名] primary key, ---字段级约束
       字段名  字段类型  default 默认值,
       字段名  字段类型  default 默认值       
       )

   表级约束语法：
       create table 用户名.表名(
       字段名  字段类型  default 默认值  [constraint 约束名] not null ,
       字段名  字段类型  default 默认值,
       字段名  字段类型  default 默认值,
       constraint 约束名 primary key(字段列表) --- 表级约束      
       )
4、外键约束
   使用foreign key进行外键约束；
   外键：在一个表中一个字段的值引用的是另外一个表的主键或者是唯一键【唯一键要求不能为null？】的值，
        这个字段就叫做外键；这个表叫做参照表；另外一个被引用的表叫做被参照表；
        外键一般用于两个表之间的关联查询的关联条件；
        
        当被参照表中的被参照记录被删除时，参照表中的外键有三种处理方式：
        no action(默认的)：如果该记录被引用，则不允许删除；
        set null：外键值设置为 null 值；
        cascade：被参照记录，外键引用的记录也一块被删除。（连坐）（级联删除）
    
    列级约束语法：
       create table 用户名.表名(
       字段名  字段类型  default 默认值 
            [constraint 约束名] references 被参照的表名(被参照的字段名) ON DELETE 外键操作类型, ---字段级约束
       字段名  字段类型  default 默认值,
       字段名  字段类型  default 默认值       
       )

   表级约束语法：
       create table 用户名.表名(
       字段名  字段类型  default 默认值  [constraint 约束名] not null ,
       字段名  字段类型  default 默认值,
       字段名  字段类型  default 默认值,
       constraint 约束名 primary key(字段列表), --- 主键表级约束 
       --- 外键表级约束     
       [constraint 约束名] foreign key(外键字段名) references 被参照的表名(被参照的字段名) ON DELETE 外键操作类型
       
       )
     
     注意：
          外键约束的操作类型有三种：什么也不写是默认的no action操作、set null、cascade级联删除操作；
          外键约束，在进行插入操作之前会进行检测，如果插入的记录中的外键字段的值，在被参照表中没有找到，则拒绝插入；

5、自定义约束
   使用check进行自定义约束；
   使用check对字段的值进行限定；
   列级约束语法：
       create table 用户名.表名(
       字段名  字段类型  default 默认值 
            [constraint 约束名] check(约束条件表达式), ---字段级约束
       字段名  字段类型  default 默认值,
       字段名  字段类型  default 默认值       
       )

   表级约束语法：
       create table 用户名.表名(
       字段名  字段类型  default 默认值  [constraint 约束名] not null ,
       字段名  字段类型  default 默认值,
       字段名  字段类型  default 默认值,
       constraint 约束名 primary key(字段列表), --- 主键表级约束 
       --- 外键表级约束     
       [constraint 约束名] foreign key(外键字段名) references 被参照的表名(被参照的字段名) ON DELETE 外键操作类型,
       --- check约束
       [constraint 约束名] check(约束条件表达式)
       )
6、约束的操作
   （1）添加约束
      语法：
          alter table  用户名.表名 
                add [constraint 约束名] 约束类型(约束字段或表达式);
   （2）删除约束
       语法：
           alter table  用户名.表名 
                drop primary key | unique(字段) | constraint 约束名 [cascade]
       注意：
           如果是cascade会进行级联删除；例如，删除一个主键，如果该主键是另外一个表的外键，则一块删除；
   （3） 启用或禁用
       禁用语法：
           alter table 用户名.表名
                 disable constraint 约束名 [cascade];
       启用语法：
           alter table 用户名.表名
                 enable constraint 约束名;
7、从数据字段查询约束对象信息
   select * from  user_constraints where upper(table_name) = upper('表名');
   
             
--------------------------------*/
-----完整性约束
---主键约束
-- 列级约束
create table depts10 (
dept_id number(4) constraint depts11_deptId_pk primary key,
dept_name varchar2(20),
manager_fname  varchar2(20),
dept_loc  varchar2(30)
);

--- 插入数据（主键值不能为null）
insert into depts10(dept_id,dept_name,manager_fname,dept_loc)
       values(null,'开发部','xiaming','300000');

-- 表级约束
create table depts11 (
dept_id number(4) ,
dept_name varchar2(20),
manager_fname  varchar2(20),
dept_loc  varchar2(30),
-- 追加表级约束
constraint depts11_deptId_pk  primary key(dept_id)
);

--- 插入数据（主键值不能为null）
insert into depts11(dept_id,dept_name,manager_fname,dept_loc)
       values(1001,'开发部','xiaming','300000');

---- 外键约束

--- 默认操作--------------------------------------------
-- 被参照的表
create table depts12 (
dept_id number(11) constraint depts12_deptId_pk primary key,
dept_name varchar2(20),
manager_id  number(11),
dept_loc  number(11)
);
--- 插入数据
insert into depts12(dept_id,dept_name,manager_id,dept_loc) 
select * from departments;

--- 参照表
create table emps12 (
   emp_id number(11) constraint emps12_empId_pk primary key,
   emp_name varchar2(50) ,
   dept_id number(11) references depts12(dept_id)  --- 列级约束
);
-- 插入数据
insert into emps12 (emp_id,emp_name,dept_id) 
select employee_id,first_name || ' ' || last_name,department_id from employees;

-- 查看数据
select * from depts12;
select * from emps12;
--- 默认操作
-- 参数被参照表中的部门id 为60的记录
-- 如果在参照表中有引用，则不能删除。
-- 如果有子记录，则不能删除
delete from depts12 where dept_id = 60;
--- 默认操作--------------------------------------------

--- set null操作--------------------------------------------
-- 被参照的表
create table depts13 (
dept_id number(11) constraint depts13_deptId_pk primary key,
dept_name varchar2(20),
manager_id  number(11),
dept_loc  number(11)
);
--- 插入数据
insert into depts13(dept_id,dept_name,manager_id,dept_loc) 
select * from departments;

--- 参照表
create table emps13 (
   emp_id number(11) constraint emps13_empId_pk primary key,
   emp_name varchar2(50) ,
   dept_id number(11) constraint emps13_deptId_fk  references depts13(dept_id) ON DELETE set null  --- 列级约束
);
-- 插入数据
insert into emps13 (emp_id,emp_name,dept_id) 
select employee_id,first_name || ' ' || last_name,department_id from employees;

-- 提交事务
commit;
-- 查看数据
select * from depts13;
select * from emps13;
--- 默认操作
-- 参数被参照表中的部门id 为60的记录
-- 如果在参照表中有引用，则不能删除。
-- 如果有子记录，则不能删除
delete from depts13 where dept_id = 60;

--- set null操作--------------------------------------------

--- cascade 级联删除操作--------------------------------------------
-- 被参照的表
create table depts14 (
dept_id number(11) constraint depts14_deptId_pk primary key,
dept_name varchar2(20),
manager_id  number(11),
dept_loc  number(11)
);
--- 插入数据
insert into depts14(dept_id,dept_name,manager_id,dept_loc) 
select * from departments;

--- 参照表
create table emps14 (
   emp_id number(11) constraint emps14_empId_pk primary key,
   emp_name varchar2(50) ,
   dept_id number(11) constraint emps14_deptId_fk  references depts14(dept_id) ON DELETE cascade  --- 列级约束
);
-- 插入数据
insert into emps14 (emp_id,emp_name,dept_id) 
select employee_id,first_name || ' ' || last_name,department_id from employees;

-- 提交事务
commit;
-- 查看数据
select * from depts14;
select * from emps14;

--- 默认操作
-- 参数被参照表中的部门id 为60的记录
-- 如果在参照表中有引用，则不能删除。
-- 如果有子记录，则不能删除
delete from depts14 where dept_id = 60;

--- cascade 级联删除操作--------------------------------------------

---外键约束，在进行插入操作之前会进行检测，如果插入的记录中的外键字段的值，在被参照表中没有找到，则拒绝插入；
insert into emps14 (emp_id,emp_name,dept_id) 
       values(3000,'xiaoming',9000); --- 拒绝插入，因为在被参照表depts14中没有dept_id 为9000的记录；

-- check自定义约束
create table emps15 (
   emp_id number(11) constraint emps15_empId_pk primary key,
   emp_name varchar2(50) ,
   emp_age number(4) constraint emps15_empAge_check check(emp_age>=18)  --- 列级约束
);

-- 在进行新增操作时，会进行check约束检测
insert into emps15 (emp_id,emp_name,emp_age) 
       values(1001,'小明',18);
-- 查看数据
select * from emps15;


------删除约束
-- 创建一个表
create table emps16 (
   emp_id number(11) constraint emps16_empId_pk primary key,
   emp_name varchar2(50) ,
   dept_id number(11) constraint emps16_deptId_fk  references depts14(dept_id) ON DELETE cascade,  --- 列级约束
   emp_age number(4) constraint emps16_empAge_check check(emp_age>=18)  --- 列级约束
);
-- 删除约束
alter table  emps16 
      drop constraint emps16_deptId_fk;
-- 删除主键约束
alter table  emps16 
      drop primary  key ;      
--- 查询约束
select * from  user_constraints where upper(table_name) = upper('emps16');
      
      