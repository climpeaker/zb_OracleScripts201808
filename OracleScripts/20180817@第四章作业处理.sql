/*--------------------------------
第4章作业处理：
--------------------------------*/
---作业处理P93
select * from departments;
select count(*),count(manager_id) from departments d;

---- test06
/*
以下三个表
s表：sno，sname
c表：cno，cname
sc表：sno，cno，scgrade
*/
-- 有两门以上（含两门）不及格课程的学生姓名及其平均成绩
--(1)查询有两门不及格学生的学号
select sno from sc where scgrade < 60 group by sno having count(*) >=2
---（2）计算学生的平均成绩
select avg(nvl(scgrade,0)) from sc group by sno
--- (3) 计算所有两门及以上不及格学生的平均成绩
select avg(nvl(scgrade,0)) from sc where sno in (
                   select sno from sc where scgrade < 60 group by sno having count(*) >=2
       )group by sno

--- (4) 显示学生姓名(最终结果)       
select s.sname, avg(nvl(scgrade,0)) as avgScgrade from s,sc where s.sno = sc.sno and sno in (
              select sno from sc where scgrade < 60 group by sno having count(*) >=2
       )group by sc.sno,s.sname

--既学过语文有学过英语的所有的学生姓名
--- 解法一
-- （1） 查询所有学过语文的学生的学号
select sno from c,sc where sc.cno = c.cno and c.cname = '语文'
-- （2） 查询所有学过英语的学生的学号
select sno from c,sc where sc.cno = c.cno and c.cname = '英语'
-- （3） 查询所有学过英语和语文的学生的姓名
select sname from s where sno in (select sno from c,sc where sc.cno = c.cno and c.cname = '语文')
       and sno in (select sno from c,sc where sc.cno = c.cno and c.cname = '英语')
       
---解法二
--（1）按学号分组，统计该学生所学习的课程数目, = 2 表示既学过英语又学过语文
select sno from sc where cno in (
                select cno from c where cname in ('英语','语文')
           ) group by sno having count(*) = 2;
-- （2）查询学生姓名
select sname from s where s.sno in ( -- 子查询
       select sno from sc where cno in (
                select cno from c where cname in ('英语','语文')
           ) group by sno having count(*) = 2
)
